

export const configuration = {
    mode:"uat",
    baseURL:{
      //dev:"https://api.myjson.com/bins/cxpnd",
      dev:"http://httpbin.org/post",
      uat:"https://cwp-uat.prudential.co.th",
      prod:"https://prupolicy.prudential.co.th"
    },
    webServiceEndpoint:{
      dev:"",
      uat:"/PolicyBackService/submit-claim",
      prod:"/PolicyBackService/submit-claim"
    },
    appPath:{
      dev:"",
      uat:"",
      prod:""
    },
    cdn:{
      dev: process.env.PUBLIC_URL,
      uat: process.env.PUBLIC_URL,
      prod:process.env.PUBLIC_URL,
    },
    defaultLanguage:"TH",
    app:{
      homeLink:"/CWPWeb/mydata/mydata-registerdata.htm#nbb",
      callback:"/CWPWeb/policy/policy-listInvestment.htm",
    },
    maxFileSize : 25 //MB,
    ,version : "1.6.4" // Please also update logs in the Markdown file (README.md). 
    ,timeout : 60000 // 1000 = 1 second, 60 seconds
  }
  