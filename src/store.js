import { createStore, combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';





const initialState = {
  occ: null,
  age: null,
  gender:null,
  companyname: '',
  name: '',
  mop:12,
  companytype: '',
  language: 'th'}





const user = (state = initialState, action) =>{

    
  switch(action.type) {
    case 'SETLANGUAGE':
    return {
      ...state,
      language: action.language
    }
    default:
      return state
  }
}




const reducer = combineReducers({
  user,
  form: reduxFormReducer, // mounted under "form"
});
const store = (window.devToolsExtension
  ? window.devToolsExtension()(createStore)
  : createStore)(reducer);

export default store;
