import React from 'react'
import {configuration} from '../configuration'

const Footer = (props)=>(
    <footer className="site-footer">
            <div className="container">
                <div className="copyright has-text-centered">
                    สงวนลิขสิทธิ์ © 2561 <br/>บมจ. พรูเด็นเชียล ประกันชีวิต (ประเทศไทย)
                </div>
                <div className="footer-version has-text-centered">
                     {configuration.version} {configuration.mode !== "prod" && configuration.mode} 
                </div>
            </div>
	</footer>  
)
export default Footer;