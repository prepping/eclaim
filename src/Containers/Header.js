import React,{Component} from 'react'
import { connect } from 'react-redux'
import {setLanguage} from '../reducer/action';
//import { log } from 'util';
const logo = require('../assets/images/pru-logo.png');

/**
* Header
* ! warn
* ? doubt
* TODO: What?
* @param 
*/



class Header extends Component{

   toggleLanguage =()=>{
    if(this.props.user.language==="th"){
    this.props.setLanguage("en")
    }else{
      this.props.setLanguage("th")
    }
      
  }

   render(){

    return (
            <header className="site-header">
            <div className="container upper-header has-text-centered">
              <img src={logo} className="pru-logo" alt="Logo" />
            
              <div className="language-switcher">
              <span onClick={this.toggleLanguage}>{this.props.user.language==="th"? "EN": "TH"}</span>
            </div>
            </div>
          
          </header>
    )
  }

}


const mapStateToProps = state => {
  const { user } = state
  return {
      user
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setLanguage:(lang) => dispatch(setLanguage(lang))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
