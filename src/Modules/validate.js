import React from "react";
import {FormattedMessage} from 'react-intl';


import {configuration} from '../configuration'



const validate = values => {
  const errors = {};
  if (!values.full_name) {
    errors.full_name = <FormattedMessage id="app.error.required"/>;
  }
  if (!values.lastName) {
    errors.lastName = <FormattedMessage id="app.error.required"/>;
  }
  if (!values.policy_no) {
    errors.policy = <FormattedMessage id="app.error.required"/>;
  }

/*   if (!values.date_of_birth) {
    errors.date_of_birth = <FormattedMessage id="app.error.required"/>;
  } */

  if (!values.claimant) {
    errors.claimant = <FormattedMessage id="app.error.required"/>;
  }

  if (!values.claimant_other) {
    errors.claimant_other = <FormattedMessage id="app.error.required"/>;
  }

  if (!values.claim_type) {
    errors.claim_type = <FormattedMessage id="app.error.required"/>;
  }


  if (!values.files) {
    errors.files = <FormattedMessage id="app.error.requiredFile"/>; 
  }else{
     if(values.files.length < 1){    
      errors.files = <FormattedMessage id="app.error.requiredFile"/>; 
    }else{

      let sumFileSize = 0
      for(let file of values.files){
        sumFileSize+=file.size
      }
        if(sumFileSize > (1024*1024*configuration.maxFileSize)){
          errors.files = <FormattedMessage id="app.error.overfilesize"/>; 

        }
    } 

  }





  if (!values.file_bank) {
    errors.file_bank = <FormattedMessage id="app.error.requiredFile"/>; 
  }else{
     if(values.file_bank.length < 1){    
      errors.file_bank = <FormattedMessage id="app.error.requiredFile"/>; 
    }else{

      let sumFileSize = 0
      for(let file of values.file_bank){
        sumFileSize+=file.size
      }
        if(sumFileSize > (1024*1024*6)){
          errors.file_bank = <FormattedMessage id="app.error.overfilesize"/>; 

        }
    } 

  }







  if (!values.email) {
    errors.email = <FormattedMessage id="app.error.required"/>;
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = <FormattedMessage id="app.error.wrongFormat"/>;
  }
  if (!values.sex) {
    errors.sex = <FormattedMessage id="app.error.required"/>;
  }
  if (!values.favoriteColor) {
    errors.favoriteColor = <FormattedMessage id="app.error.required"/>;
  }

  if (!values.payment_type) {
    errors.payment_type = <FormattedMessage id="app.error.required"/>;
  }

/*   if (!values.file_bank || values.file_bank.length < 1) {
    errors.file_bank = <FormattedMessage id="app.error.requiredFile"/>;
  } */

  //PhoneNumber
  if (!values.phone) {
    errors.phone = <FormattedMessage id="app.error.required"/>;
  } else if (!phonenumber(values.phone)) {
    errors.phone = <FormattedMessage id="app.error.wrongFormat"/>;
  }


  //Address

  if (!values.address_type) {
    errors.address_type = <FormattedMessage id="app.error.required"/>;
  }

  if (!values.address_line_1) {
    errors.address_line_1 = <FormattedMessage id="app.error.required"/>;
  }

  if (!values.address_line_2) {
    errors.address_line_2 = <FormattedMessage id="app.error.required"/>;
  }

  if (!values.address_line_3) {
    errors.address_line_3 = <FormattedMessage id="app.error.required"/>;
  }

  if (!values.address_province) {
    errors.address_province = <FormattedMessage id="app.error.required"/>;
  }


  if (!values.address_code) {
    errors.address_code = <FormattedMessage id="app.error.required"/>;
  } else if (!zipcode(values.address_code)) {
    errors.address_code = <FormattedMessage id="app.error.wrongFormat"/>;
  }



  //National ID
   if (!values.national_id) {
    errors.national_id = <FormattedMessage id="app.error.required"/>;
  } else if (!ThaiNationalID(values.national_id)) {
    errors.national_id = <FormattedMessage id="app.error.wrongFormat"/>;
  } 

  return errors;
};



function ThaiNationalID(id) {
  //return true

  if (id == null || id.length !== 13 || !/^[0-9]\d+$/.test(id)) {
    return false;
  }
  let i, sum = 0;
  for ((i = 0), (sum = 0); i < 12; i++) {
    sum += parseInt(id.charAt(i),10) * (13 - i);
  }
  let check = (11 - sum % 11) % 10;
  if (check === parseInt(id.charAt(12),10)) {
    return true;
  }
  return false;
}



function phonenumber(inputtxt) {
  //console.log(inputtxt)

  var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(inputtxt.match(phoneno)) {
    return true;
  }
  else {
    //alert("message");
    return false;
  }
}

function zipcode(input) {
  //console.log(input)

  if(input.match( /^((?!(0))[0-9]{5})$/) ) {
    return true;
  }
  else {
    //alert("message");
    return false;
  }
}



/* async function  sumFileSize(files){
  let sumFileSize = 0

    sumFileSize = files.reduce((all,item,index)=>{
      all += item.size
      return all;
    },0) 
    //Return sum from input in this feild 

   return sumFileSize

} */







export default validate;
