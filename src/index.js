//import 'babel-polyfill'
import "@babel/polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
//import { Values } from "redux-form-website-template";
import store from "./store";
//import { BrowserRouter,Route ,Switch} from 'react-router-dom';


import  Header  from './Containers/Header'
import  Footer  from './Containers/Footer'
/* import WizardFormFirstPage from './Forms/WizardFormFirstPage';
import WizardFormSecondPage from './Forms/WizardFormSecondPage';
import WizardFormThirdPage from './Forms/WizardFormThirdPage';
import WizardFormSubmit from './Forms/WizardFormSubmit'; */

import showResults from "./Modules/showResults";
import WizardForm from "./WizardForm";
import { unregister } from './registerServiceWorker';
//Language
import {  addLocaleData } from 'react-intl';
import ConnectedIntlProvider from './reducer/ConnectedIntlProvider';
import thLocaleData from 'react-intl/locale-data/th';
/* import 'bootstrap/dist/css/bootstrap.min.css' */
//import 'bulma/css/bulma.min.css'
//import "bulma-steps-component/bulma-steps.sass"
import './assets//stylesheet/bulma.css'
import './assets//stylesheet/style.css'
import './assets//stylesheet/progress.css'
//import './bundle.css';
/* import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { faTwitter } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome' */
import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { configuration } from "./configuration";
/* import { faTwitter } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome' */
library.add(fas)
// Kicks off the process of finding <i> tags and replacing with <svg>
dom.watch()
//Prevent history back
if(configuration.mode === 'prod'){
window.onbeforeunload = function() { return "Your work will be lost."; };
window.history.pushState(null, null, window.location.href);
    window.onpopstate = function () {
      window.history.go(1);
    };
  }
/* window.addEventListener('beforeunload', function (e) {
  // Cancel the event as stated by the standard.
  e.preventDefault();
  // Chrome requires returnValue to be set.
  e.returnValue = '';
}); */
/* import fontawesome from '@fortawesome/fontawesome'
import faFreeSolid from '@fortawesome/fontawesome-free-solid' */
const rootEl = document.getElementById("root");
addLocaleData(thLocaleData);
ReactDOM.render(
  <Provider store={store}>
    <ConnectedIntlProvider>
      <div>
        <Header />
        <WizardForm onSubmit={showResults} />
         {/* <Values form="wizard" /> */}
{/*         <BrowserRouter basename='/'>
				<Switch>
					<Route exact path='/' component={WizardFormFirstPage} onSubmit={(e)=>{console.log(e)}} />
					<Route exact path='/step-2' component={WizardFormSecondPage} />
				</Switch>
			</BrowserRouter> */}
        <Footer />
      </div>
    </ConnectedIntlProvider>


  </Provider>,
  rootEl
);
unregister()
