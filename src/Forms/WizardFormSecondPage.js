import React, { Component } from "react";
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form'


import validate from './../Modules/validate'

//import isEmpty from "lodash/isEmpty";
//import DropZoneField from "./../Feilds/dropzoneField";
import renderField from './../Feilds/renderField'
import selectField from './../Feilds/selectField'
//import FileInput from "./../Feilds/FileInput";

//import Dropzone from "react-dropzone";
import DropZoneField from "./../Feilds/dropzoneField";

import {configuration} from '../configuration'


import {FormattedMessage,FormattedHTMLMessage,intlShape, injectIntl} from 'react-intl';



import ThaiAddressInput from "../Feilds/ThaiAddress/ThaiAddressInput";







    
const renderFieldRadio = ({ input, label,address_type, langguage, meta: { touched, error, warning } }) => (
  <div className="field">
        <label className="radio">
          <Field name="address_type" component="input" type="radio" value="current" />
          &nbsp; <FormattedMessage id="app.user.address_type_current" />

        </label>
  
  <br/>
        <label className="radio">
          <Field name="address_type" component="input" type="radio" value="other" />
          &nbsp; <FormattedMessage id="app.user.address_type_other" />
        </label>

    {touched && error && <p className="help is-danger">{error}</p>}
  </div>
)
      









 //const imageIsRequired = value => (isEmpty(value) ? "Required" : undefined);

class WizardFormSecondPage extends Component {
  //state = { imageFile: [] ,};


  constructor(props) {
    super(props);
    this.state = {
      imageFile: [],
      maxSize : configuration.maxFileSize, //value*1024*1024  (MB)
      sumFileSize : 0

    };
   // this.gotoPage = this.gotoPage.bind(this);
}





  handleFormSubmit(formProps,index){
    const fd = new FormData();
    fd.append("imageFile", formProps.imageToUpload[0]);
    // append any additional Redux form fields
    // create an AJAX request here with the created formData
  };

/*   handleOnDrop(newImageFile,index) {

    //console.log(this.state.imageFile)
    this.state.imageFile[index]=newImageFile;
    // re-render
    this.forceUpdate();
  } */

  resetForm = () => {
    this.setState({ imageFile: [] });
    this.props.gotoPage(1);
    this.props.reset();
    
   
  };


  setFileSize(value){
    //console.log(value)
     this.setState({ sumFileSize: value });
  }




bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)),10);
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
 };

 addressCallbackHandle = (address) =>{

  //console.log(this.props)
  //console.log(address)
  //change(form:String, field:String, value:any)

  this.props.change('address_line_2', address.d)
  this.props.change('address_line_3', address.a)
  this.props.change('address_province', address.p)
 this.props.change('address_code', address.z.toString())


 }


  render = () =>{
    const {
      claim_type,address_type,intl, handleSubmit,
    } =  this.props;

   //var  placeholder_text = intl.formatMessage({id: "app.user.claim_type_option_1"});

   //console.log(placeholder_text)


   let claimOptions = [
    intl.formatMessage({id: "app.user.claim_type_option_1"}),
    intl.formatMessage({id: "app.user.claim_type_option_2"}),
    intl.formatMessage({id: "app.user.claim_type_option_3"}),
    intl.formatMessage({id: "app.user.claim_type_option_4"}),
    intl.formatMessage({id: "app.user.claim_type_option_5"}),
    intl.formatMessage({id: "app.user.claim_type_option_6"}),
        ]

   //Address data path
   let  dataPath=`${configuration.cdn[configuration.mode]}/address/${this.props.user.language}.json`


  
    return(


    <form onSubmit={handleSubmit}>

    <div className="form-body">

    {/* <Field
                      name="files"
                      label={<FormattedMessage id="app.user.profile" />}
                      textBtn={<FormattedMessage id="app.browse" />}
                      placeholder={<FormattedMessage id="app.user.profile" />}
                      accept="image/jpeg, image/png, image/gif, image/bmp"
                      component={FileInput}
                      type="file"
      /> */}


    <button
      type="button"
      className="uk-button uk-button-default uk-button-large"
      disabled={this.props.pristine || this.props.submitting}
      onClick={this.resetForm}
      style={{ float: "right" }}
    >Clear
    </button> 


      <div>
        <Field name="claim_type" 
        component={selectField} 
        options={claimOptions} 
        label={<FormattedMessage id="app.claim_type"/>}
        placeholder ={<FormattedMessage id="app.please_select"/>}
        />
      </div>


      {claim_type &&   <div className="columns">
            <div className="column">
           
            <label className="label"><span><FormattedMessage id="app.claim_docs" /></span></label>
          
{/*             <ul>

            {claim_type && optionAllow[claimOptions.indexOf(claim_type)][0] === "1" && 
            <li>ใบรับรองแพทย์ </li>}

            {claim_type && optionAllow[claimOptions.indexOf(claim_type)][1] === "1" && 
            <li>สำเนาใบเสร็จรับเงิน / ใบรายละเอียดค่าใช้จ่าย</li>}
           
           


            {claim_type && optionAllow[claimOptions.indexOf(claim_type)][2] === "1" && 
            <li>ผลเอ็กซ์เรย์ หรือ รูปถ่ายตำแหน่งที่บาดเจ็บ </li>}



            {claim_type && optionAllow[claimOptions.indexOf(claim_type)][2] === "1" && 
            <li>ใบเสร็จรับเงินต้นฉบับ (กรุณาส่งต้นฉบับทุกครั้ง)</li>}
</ul> */}
            
            {/* <li>คำแถลงของผู้เรียกร้องสิทธิเนื่องในมรณกรรมของผู้เอาประกันภัย </li>
            <li>ใบรายงานแพทย์ผู้ทำการรักษา  </li>
            <li>สำเนาใบมรณบัตรที่รับรองสำเนาถูกต้อง  </li>
            <li> สำเนาทะเบียนบ้านที่มีการจำหน่าย “ตาย”และสำเนาบัตรประชาชน</li>
            <li> ของผู้เอาประกัน  </li>
            <li> สำเนาทะเบียนบ้านและ สำเนาบัตรประชาชน ของผู้รับประโยชน์ทุกคน </li>
            <li> เอกสารเพิ่มเติมอื่นๆ  </li>
            <li>กรณีผู้รับประโยชน์เป็นผู้เยาว์ ให้ยื่นเอกสารเพิ่มเติมดังนี้</li>
            <li>สำเนาบัตรประชาชนและทะเบียนบ้านของผู้ปกครองโดยชอบธรรม 
               (กรณีไม่ใช่ บิดา มารดา กรุณาแนบสำเนาคำสั่งศาลแต่งตั้งผู้ปกครอง)</li> */}
               




               

{claim_type && claimOptions.indexOf(claim_type) ===0 &&
  <ul>
    <li><FormattedMessage id="app.claim_docs_1"/></li>        
		<li> <FormattedMessage id="app.claim_docs_2"/></li>
  </ul>
}

{claim_type && claimOptions.indexOf(claim_type) ===1 &&
  <ul>
      <li><FormattedMessage id="app.claim_docs_1"/></li>   
      <li><FormattedMessage id="app.claim_docs_3"/></li>
  </ul>
}
{claim_type && claimOptions.indexOf(claim_type) ===2 &&
  <div>
    <ul>
       <li><FormattedMessage id="app.claim_docs_4"/></li>        
       <li><FormattedMessage id="app.claim_docs_1"/></li>
       <li><FormattedMessage id="app.claim_docs_5"/></li>
       <li><FormattedMessage id="app.claim_docs_99"/></li>
       
  </ul>

  {/* <span style={{color: "red"}}>*ต้องใช้เอกสารตัวจริงเท่านั้น</span> */}

  </div>
  
}
{claim_type && claimOptions.indexOf(claim_type) ===3 &&
  <ul>
    <li><FormattedMessage id="app.claim_docs_6"/></li>
    <li> <FormattedMessage id="app.claim_docs_7"/></li>  
  </ul>
}
{claim_type && claimOptions.indexOf(claim_type) ===4 &&
  <ul>
   <li><FormattedMessage id="app.claim_docs_6"/></li>
   <li><FormattedMessage id="app.claim_docs_8"/></li>
  </ul>
}
{claim_type && claimOptions.indexOf(claim_type) ===5 &&
  <ul>
          <li><FormattedMessage id="app.claim_docs_9"/></li>
          <li><FormattedMessage id="app.claim_docs_10"/></li>
          <li><FormattedMessage id="app.claim_docs_11"/></li>
          <li><FormattedMessage id="app.claim_docs_12"/></li>
          <li><FormattedMessage id="app.claim_docs_13"/></li>
          <li><FormattedMessage id="app.claim_docs_99"/></li>
          <li>
            <FormattedHTMLMessage id="app.claim_docs_h_1"/>
            <ul>
            <li><FormattedMessage id="app.claim_docs_14"/></li>
            </ul>
          </li>
  </ul>
}


            </div>
            <div className="column">

            {claim_type  &&   claimOptions.indexOf(claim_type) !==2 &&

                <div>
                  <Field
                      name="files"
                      label={<FormattedMessage id="app.claim_browse_title" />}
                      textBtn={<FormattedMessage id="app.browse" />}
                      placeholder={""}
                      accept="image/jpeg, image/png, image/bmp, application/pdf"
                      component={DropZoneField}
                      type="file"
                      multiple={true}
                      maxSize={this.state.maxSize}
                      returnSize={(value)=>this.setFileSize(value)}
                    />
                    <span className="remark">
                    <FormattedMessage
                        id='app.files.remark' 
                        description=''
                        values={{
                            maxSize : configuration.maxFileSize,
                            size: <b className={this.state.sumFileSize>(1024*1024*(configuration.maxFileSize+1))?"has-text-danger blink":""}>{this.bytesToSize(this.state.sumFileSize)}</b>
                        }}
                    />
                    </span>
                </div>
                    

                  }


                  {claim_type  &&   claimOptions.indexOf(claim_type) ===2 &&

 
                  <div className="field">
                  {/* <label class="label">
                    กรุณาส่งเอกสารมาที่ 
                  </label>
                  พร้องระบุ ref.no */}
                  </div>

                  }

      


           
            
           
            </div>
       
          </div>

     }




          <div className="columns ">
            <div className="column ">

              <label className="label"><FormattedMessage id="app.user.address_type" /></label>


            </div>
            <div className="column is-four-fifths">



              <Field
                name="address_type"
                address_type={address_type}
                component={renderFieldRadio}
                language={this.props.user.language}

              />








            </div>

          </div>











          {address_type && address_type === "other" &&
          <div>
                  <div className="columns ">
                    <div className="column">
                      <Field
                        name="address_line_1"
                        type="text"
                        component={renderField}
                        label={<FormattedMessage id="app.user.address_line_1" />}
                        placeholder=""
                      /></div>
                  </div>

             <div className="columns ">
                <div className="column">
                <Field
                  name="address_line_2"
                  type="text"
                  dataPath= {dataPath}
                  component={ThaiAddressInput}
                  addressType = "d"
                  language={this.props.user.language}
                  label={<FormattedMessage id="app.user.address_line_2" />}
                  limit={20}
                  placeholder=""
                  callback={this.addressCallbackHandle}
                />   
                </div>
                <div className="column">
                <Field
                  name="address_line_3"
                  type="text"
                  dataPath= {dataPath}
                  component={ThaiAddressInput}
                  addressType = "a"
                  language={this.props.user.language}
                  label={<FormattedMessage id="app.user.address_line_3" />}
                  limit={20}
                  placeholder=""
                  callback={this.addressCallbackHandle}
                />  
                </div>
            </div>

            <div className="columns ">
                <div className="column">
                <Field
                  name="address_province"
                  type="text"
                  dataPath= {dataPath}
                  component={ThaiAddressInput}
                  addressType = "p"
                  language={this.props.user.language}
                  label={<FormattedMessage id="app.user.address_province" />}
                  limit={20}
                  placeholder=""
                  callback={this.addressCallbackHandle}
                />  
                </div>
                <div className="column">
                <Field
                  name="address_code"
                  type="text"
                  dataPath= {dataPath}
                  component={ThaiAddressInput}
                  addressType = "z"
                  language={this.props.user.language}
                  label={<FormattedMessage id="app.user.address_code" />}
                  limit={20}
                  placeholder=""
                  maxlenght = '5'
                  match={/^(\s*|\d+)$/i}
                  callback={this.addressCallbackHandle}
                />  
                </div>
            </div>

                
           </div>
             } 




          {address_type && address_type === "otherXXX" &&
          <div>

            <div className="columns ">
              <div className="column">
                <Field
                  name="address_line_1"
                  type="text"
                  component={renderField}
                  label={<FormattedMessage id="app.user.address_line_1" />}
                  placeholder=""
                /></div>
            </div>

            <div className="columns ">
              <div className="column">
                <Field
                  name="address_line_2"
                  type="text"
                  component={renderField}
                  label={<FormattedMessage id="app.user.address_line_2" />}
                  placeholder=""
                /></div>
              <div className="column">
                <Field
                  name="address_line_3"
                  type="text"
                  component={renderField}
                  label={<FormattedMessage id="app.user.address_line_3" />}
                  placeholder=""
                /></div>
            </div>


            <div className="columns ">
              <div className="column">
                <Field
                  name="address_province"
                  type="text"
                  component={renderField}
                  label={<FormattedMessage id="app.user.address_province" />}
                  placeholder=""
                /></div>
              <div className="column">
                <Field
                  name="address_code"
                  type="number"
                  component={renderField}
                  label={<FormattedMessage id="app.user.address_code" />}
                  maxlenght = '5'
                  placeholder=""
                /></div>
            </div>
            
                    

                 </div>  
                  }




        </div>

        <div className="paging">





<div className="field is-grouped is-grouped-centered">
<p className="control">
<span className="button is-light is-medium" onClick={this.props.previousPage}>
<FormattedMessage id="app.previous" />
</span>
</p>
<p className="control">
<button type="submit" className="button is-pru is-medium">
<FormattedMessage id="app.next" />
</button>
</p>
</div>



</div>

        </form>
    
  );
}
}


WizardFormSecondPage.propTypes = {
  intl: intlShape.isRequired
}

// Decorate with redux-form
WizardFormSecondPage = reduxForm({
  form: 'wizard', //Form name is same
  destroyOnUnmount: false,
  multipartForm : true,
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
})(WizardFormSecondPage)

// Decorate with connect to read form values


const selector = formValueSelector('wizard') // <-- same as form name
WizardFormSecondPage = connect(
  state => {
    // can select values individually
    const claim_type = selector(state, 'claim_type')
    const address_type = selector(state, 'address_type')
    // or together as a group
    //const { firstName, lastName } = selector(state, 'firstName', 'lastName')
    //MAP USER STATE
    const { user } = state
    return {
      user,
      claim_type,
      address_type,
    }
  }
)(WizardFormSecondPage)

//export default WizardFormSecondPage

export default injectIntl(WizardFormSecondPage);










/* export default reduxForm({
  form: 'wizard', //Form name is same
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
})(WizardFormSecondPage) */
