import React, { Component } from "react";
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import {FormattedMessage, injectIntl} from 'react-intl';
import validate from './../Modules/validate'
import renderField from './../Feilds/renderField'
import DateInput from './../Feilds/DateInput'
/* import FileInput from "./../Feilds/FileInput";
import Dropzone from "react-dropzone";
import DropZoneField from "./../Feilds/dropzoneField"; */
//Date Picker
//import DatePicker from 'react-datepicker';
import moment from 'moment';
//import th from 'moment/locale/th';
const renderFieldRadio = ({ input,intl, label,connect_claimant, language, type, meta: { touched, error, warning } }) => (
  <div className="field">
        <label className="label"><FormattedMessage id="app.user.claimant" /></label>
        <div className="columns">
          <div className="column ">
            <label className="radio">
              <Field name="claimant" component="input" type="radio" value={intl.formatMessage({id: "app.user.claimant_option_1"})} />
              &nbsp; {intl.formatMessage({id: "app.user.claimant_option_1"})}
   
    </label>
          </div>
          <div className="column ">
            <label className="radio">
            <Field name="claimant" component="input" type="radio" value={intl.formatMessage({id: "app.user.claimant_option_2"})} />
            &nbsp; {intl.formatMessage({id: "app.user.claimant_option_2"})}
    </label>
          </div>
          <div className="column ">
            <label className="radio">
            <Field name="claimant" component="input" type="radio" value={intl.formatMessage({id: "app.user.claimant_option_3"})} />
            &nbsp; {intl.formatMessage({id: "app.user.claimant_option_3"})}
    </label>
          </div>
        </div>
        <div className="columns">
          <div className="column ">
            <label className="radio">
            <Field name="claimant" component="input" type="radio" value={intl.formatMessage({id: "app.user.claimant_option_4"})} />
            &nbsp; {intl.formatMessage({id: "app.user.claimant_option_4"})}
    </label>
          </div>
          <div className="column is-two-thirds claimant-other">
            <label className="radio">
            <Field name="claimant" component="input" type="radio" value={intl.formatMessage({id: "app.user.claimant_option_5"})} />
            &nbsp; {intl.formatMessage({id: "app.user.claimant_option_5"})}
    </label>&nbsp;
    {connect_claimant === intl.formatMessage({id: "app.user.claimant_option_5"}) &&
              <Field
                name="claimant_other"
                type="text"
                className="input"
                component={renderField}
                placeholder={<FormattedMessage id="app.please_fill" />}
              />
              
            }
          </div>
         
        </div>
        {touched && error && <p className="help is-danger">{error}</p>}
      </div>
)
/* const renderError = ({ meta: { touched, error } }) =>
  touched && error ? <span>{error}</span> : false; */
  class WizardFormFirstPage extends Component {
/* const WizardFormFirstPage = props => {
  const { handleSubmit,user } = props */
  
  //console.log(user.language)
  componentWillMount(){
}
activeThaiYear(){
    //moment.locale('en-gb')
    window.yearAdd = 543
    const thaiSolarFormat = 'YYYY';
    const checkThaiSolarFormat = format => {
      return /YYYY/.test(format);
    };
    const convertThaiSolarYear = (year) => {
      return parseInt(year, 10) + 543;
    }
    const format = moment.fn.format;
    //const year = moment.fn.year;
    moment.fn.format = function (str) {
      if (str) {
        if (checkThaiSolarFormat(str)) {
          str = str.replace(thaiSolarFormat, convertThaiSolarYear(this.get('year')));
        }
      }
      return format.call(this, str);
    }
}

onSubmit(values) {
  //this.props.createPost(values, () => {
    this.props.history.push('/step-2')
 // });
}
  
  render = () =>{
    const { handleSubmit,user,connect_claimant } = this.props
   // console.log(connect_claimant)
    if(user.language==="th"){
      this.activeThaiYear();
    }else{
      window.yearAdd = 0
    }
  return (
 
<form onSubmit={handleSubmit}> 
{/* <form onSubmit={handleSubmit(this.onSubmit.bind(this))}> */}




      {/* <Field name="picture" component={props =>
    <Dropzone
       {...props.input}
        multiple={false} 
        onDrop={(filesToUpload) => {
         this.files = filesToUpload;
         return props.input.onChange(filesToUpload);
         }}
    >
         <div>Try dropping a file here, or click to select file to upload.</div>
    </Dropzone>
    } type="file"/>
    {this.files &&
    <div>
        {this.files.map((file, i) => <span key={i}>{file.name}</span>)}
    </div>
     } */}
     <div className="form-body">
{/*      <Field
                      name="files"
                      label={<FormattedMessage id="app.user.profile" />}
                      textBtn={<FormattedMessage id="app.browse" />}
                      placeholder={<FormattedMessage id="app.user.profile" />}
                      accept="image/jpeg, image/png, image/gif, image/bmp"
                      component={FileInput}
                      type="file"
 /> */}
     
     <div className="columns">
        <div className="column is-half">
          <Field
            name="full_name"
            type="text"
            component={renderField}
            label={<FormattedMessage id="app.user.full_name" />}
            placeholder={<FormattedMessage id="app.user.full_name_placeholder" />}
            icon="user"
          />
        </div>
        <div className="column is-half">
          <Field
            name="national_id"
            type="tel"
            component={renderField}
            label={<FormattedMessage id="app.user.national_id" />}
            placeholder={<FormattedMessage id="app.user.national_id_placeholder" />}
            icon="address-card"
            match={/^(\s*|\d+)$/i}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-half">
          <Field
            name="date_of_birth"
            type="text"
            component={DateInput}
            lang={user.language}
            label={<FormattedMessage id="app.user.date_of_birth" />}
            placeholder={<FormattedMessage id="app.user.date_of_birth_placeholder" />}
          />
        </div>
        <div className="column is-half">
          <Field
            name="policy_no"
            type="text"
            component={renderField}
            label={<FormattedMessage id="app.user.policy_no" />}
            placeholder={<FormattedMessage id="app.user.policy_no_placeholder" />}
            icon="file-invoice"
            match={/^(\s*|[A-Za-z0-9]+)$/i}
            
           // maxlenght='5'
          />
        </div>
      </div>
     
        <Field
            name="claimant"
            connect_claimant={connect_claimant}
            component={renderFieldRadio}
            language={this.props.user.language}
            intl ={this.props.intl}
          />
      <div className="columns">
        <div className="column is-half">
          <Field
            name="phone"
            type="tel"
            component={renderField}
            label={<FormattedMessage id="app.user.phone" />}
            placeholder={<FormattedMessage id="app.user.phone_placeholder" />}
            icon="mobile-alt"
          />
        </div>
        <div className="column is-half">
          <Field
            name="email"
            type="email"
            component={renderField}
            label={<FormattedMessage id="app.user.email" />}
            placeholder={<FormattedMessage id="app.user.email_placeholder" />}
            icon="envelope"
          />
        </div>
      </div>
     
     
     </div>
      <div className="paging">
{/*       <button
      type="button"
      className="uk-button uk-button-default uk-button-large"
      disabled={this.props.pristine || this.props.submitting}
      onClick={this.resetForm}
      style={{ float: "right" }}
    >Clear
    </button> */}
    <div className="field is-grouped is-grouped-centered">
  <p className="control">
    <button type="submit" className="button is-pru is-medium">
    <FormattedMessage id="app.next" />
    </button>
  </p>
</div>
      </div>
    </form>
  )
}
  }
/* const mapStateToProps = state => {
  const { user } = state
  return {user}
}
 */
/* export default connect(mapStateToProps)(reduxForm({
  form: 'wizard', // <------ same form name
  destroyOnUnmount: false, // <------ preserve form data
  multipartForm : true,
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
})(WizardFormFirstPage)) */
// Decorate with redux-form
WizardFormFirstPage = reduxForm({
  form: 'wizard', //Form name is same
  destroyOnUnmount: false,
  multipartForm : true,
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
})(WizardFormFirstPage)
// Decorate with connect to read form values
const selector = formValueSelector('wizard') // <-- same as form name
WizardFormFirstPage = connect(
  state => {
    // can select values individually
    const connect_claimant = selector(state, 'claimant')
    const type = selector(state, 'type')
    // or together as a group
    const { firstName, lastName } = selector(state, 'firstName', 'lastName')
    //MAP USER STATE
    const { user } = state
    return {
      user,
      connect_claimant,
      type,
      fullName: `${firstName || ''} ${lastName || ''}`
    }
  }
)(WizardFormFirstPage)
//export default WizardFormFirstPage
export default injectIntl(WizardFormFirstPage);
//export default connect(mapStateToProps, mapDispatchToProps)(WizardForm)
