import React, { Component } from "react";
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form'


import validate from './../Modules/validate'


//import isEmpty from "lodash/isEmpty";
//import DropZoneField from "./../Feilds/dropzoneField";
//import renderField from './../Feilds/renderField'
//import selectField from './../Feilds/selectField'
//import FileInput from "./../Feilds/FileInput";

//import Dropzone from "react-dropzone";
import DropZoneField from "./../Feilds/dropzoneField";


import {FormattedMessage } from 'react-intl';



const renderFieldRadio = ({ input, label,payment_type, langguage, meta: { touched, error, warning } }) => (
  <div className="field">
        <label className="radio">
          <Field name="payment_type" component="input" type="radio" value="cheque" />
          &nbsp; <FormattedMessage id="app.user.payment_type_cheque" />

        </label>
  
  <br/>
        <label className="radio">
          <Field name="payment_type" component="input" type="radio" value="bank" />
          &nbsp; <FormattedMessage id="app.user.payment_type_bank" />
        </label>

    {touched && error && <p className="help is-danger">{error}</p>}
  </div>
)





class WizardFormThirdPage extends Component {
  state = { imageFile: [] , sumFileSize : 0 };



  handleFormSubmit(formProps,index){
    const fd = new FormData();
    fd.append("imageFile", formProps.imageToUpload[0]);
    // append any additional Redux form fields
    // create an AJAX request here with the created formData
  };



  resetForm = () => {
    this.setState({ imageFile: [] });
    this.props.gotoPage(1);
    this.props.reset();
    
   
  };


  bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)),10);
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
 };


 setFileSize(value){
  this.setState({ sumFileSize:value});

 }

  render = () =>{
    


    const {
      payment_type, handleSubmit,
    } =  this.props;


  
    return(


    <form onSubmit={handleSubmit}>

    <div className="form-body">


      <div className="columns ">
            <div className="column ">
           
            <label className="label"><FormattedMessage id="app.user.payment_type"/></label>


            </div>
            <div className="column is-four-fifths">



            <Field
            name="payment_type"
            payment_type={payment_type}
            component={renderFieldRadio}
            language={this.props.user.language}
          />




            {payment_type && payment_type === "bank" &&
                    <div style={{marginLeft:"20px"}}><Field
                      name="file_bank"
                      textBtn={<FormattedMessage id="app.browse" />}
                      placeholder={<FormattedMessage id="app.user.profile" />}
                      accept="image/jpeg, image/png, image/bmp, application/pdf"
                      component={DropZoneField}
                      multiple={false}
                      maxSize={5}
                      type="file"
                      returnSize={(value)=>this.setFileSize(value)}
                    />

                  <span className="remark">
                    <FormattedMessage
                    id='app.files.remark' 
                    description=''
                    values={{
                        maxSize : 5,
                        size: <b className={this.state.sumFileSize>(1024*1024*6)?"has-text-danger blink":""}>{this.bytesToSize(this.state.sumFileSize)}</b>
                    }}
                    />
                    </span>
                    
                    
                    </div>
                  }


  
            </div>
       
          </div>

        </div>

        <div className="paging">





<div className="field is-grouped is-grouped-centered">
<p className="control">
<span className="button is-light is-medium" onClick={this.props.previousPage}>
<FormattedMessage id="app.previous" />
</span>
</p>
<p className="control">
<button type="submit" className="button is-pru is-medium">
<FormattedMessage id="app.next" />
</button>
</p>
</div>



</div>

        </form>
    
  );
}
}




// Decorate with redux-form
WizardFormThirdPage = reduxForm({
  form: 'wizard', //Form name is same
  destroyOnUnmount: false,
  multipartForm : true,
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
})(WizardFormThirdPage)

// Decorate with connect to read form values


const selector = formValueSelector('wizard') // <-- same as form name
WizardFormThirdPage = connect(
  state => {
    // can select values individually
    const payment_type = selector(state, 'payment_type')
    const type = selector(state, 'type')
    // or together as a group
    //const { firstName, lastName } = selector(state, 'firstName', 'lastName')
    //MAP USER STATE
    const { user } = state
    return {
      user,
      payment_type,
      type,
    }
  }
)(WizardFormThirdPage)

export default WizardFormThirdPage



