import React, { Component } from "react";
import { connect } from 'react-redux'
import { reduxForm, formValueSelector } from 'redux-form'
import validate from './../Modules/validate'
//import isEmpty from "lodash/isEmpty";
//import DropZoneField from "./../Feilds/dropzoneField";
//import renderField from './../Feilds/renderField'
//import selectField from './../Feilds/selectField'
//import FileInput from "./../Feilds/FileInput";
//import Dropzone from "react-dropzone";
//import DropZoneField from "./../Feilds/dropzoneField";
import {FormattedMessage,FormattedHTMLMessage} from 'react-intl';
class WizardFormSubmit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status : "loading",
      ref_no : ""
    };
   // this.gotoPage = this.gotoPage.bind(this);
}
  //state = { imageFile: [] , status : "loading" };
  handleFormSubmit(formProps,index){
    const fd = new FormData();
    fd.append("imageFile", formProps.imageToUpload[0]);
    // append any additional Redux form fields
    // create an AJAX request here with the created formData
  };
/*   handleOnDrop(newImageFile,index) {
    console.log(this.state.imageFile)
    this.state.imageFile[index]=newImageFile;
    // re-render
    this.forceUpdate();
  } */
  resetForm = () => {
    this.setState({ imageFile: [] });
    this.props.gotoPage(1);
    this.props.reset();
  };
  componentDidMount(){
   // console.log(this.props.handleSubmit())
    this.props.handleSubmit().then(response => {
      console.log(response)
      //console.log("success")
     // "data":{"refNo":"CLM1539835109097"}}
      if(response.status==="success"){
        this.setState({
          status:"success",
          ref_no : response.data.refNo
        })
      }else{
        this.setState({
          status:"error"
        })
      }
    }).catch(err => {
      console.log(err)
      this.setState({
        status:"error"
      })
    }); 
    //this.props.onSubmit();
  }
  render = () =>{
    const {handleSubmit} =  this.props;
    return(
      <form onSubmit={handleSubmit}>
        <div className="form-body status">
          {this.state.status === "loading" &&
            <div>
              <span className="icon has-text-danger">
                <i className="fas fa-spinner fa-pulse fa-3x"></i>
              </span>
              <div className="details">
                <FormattedMessage id="app.sending" />
              </div>
            </div>
          }
          {this.state.status === "success" &&
            <div>
              <span className="icon has-text-success">
                <i className="fas fa-check-circle fa-3x"></i>
              </span>
              <div className="details">
                <FormattedHTMLMessage id="app.success" />
                <br /><br />
                <div className="columns">
                  <div className="column is-half is-offset-one-quarter">
                    <div className="notification">
                      <FormattedHTMLMessage id="app.ref.notice" /><br /><br />
                      <div className="button is-danger ref-no">
                        {this.state.ref_no}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          }
          {this.state.status === "error" &&
            <div>
              <span className="icon has-text-danger">
                <i className="fas fa-exclamation-triangle fa-3x"></i>
              </span>
              <div className="details">
                <FormattedHTMLMessage id="app.failed" />
              </div>
            </div>
          }
        </div>
        <div className="paging">
{/*           <button
            type="button"
            className="uk-button uk-button-default uk-button-large"
            disabled={this.props.pristine || this.props.submitting}
            onClick={this.resetForm}
            style={{ float: "right" }}
          >Clear
</button>
 */}
       {/*  {this.state.status === "error" && 
        <div className="field is-grouped is-grouped-centered">
            <p className="control">
              <span className="button is-light is-medium" onClick={this.props.previousPage}>
                <FormattedMessage id="app.previous" />
              </span>
            </p>
          </div>
          } */}
            {this.state.status === "error" && 
                  <div className="field is-grouped is-grouped-centered">
                      <p className="control">
                        <span className="button is-danger is-medium" onClick={this.props.previousPage}>
                        <FormattedMessage id="app.back" />
                      </span>
                      </p>
                    </div>
                    }
            {this.state.status === "success" && 
                  <div className="field is-grouped is-grouped-centered">
                      <p className="control">
                        <a className="button is-pru is-medium" href="https://www.prudential.co.th/">
                        <FormattedMessage id="app.finished" />
                      </a>
                      </p>
                      <p className="control">
                        <span className="button is-medium" onClick={()=>window.print()}>
                        <i className="fas fa-print"></i>&nbsp; Print
                      </span>
                      </p>
                    </div>
                    } 
        </div>
      </form>
  );
}
}
// Decorate with redux-form
WizardFormSubmit = reduxForm({
  form: 'wizard', //Form name is same
  destroyOnUnmount: false,
  multipartForm : true,
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
})(WizardFormSubmit)
// Decorate with connect to read form values
const selector = formValueSelector('wizard') // <-- same as form name
WizardFormSubmit = connect(
  state => {
    // can select values individually
    const payment_type = selector(state, 'payment_type')
    const type = selector(state, 'type')
    // or together as a group
    //const { firstName, lastName } = selector(state, 'firstName', 'lastName')
    //MAP USER STATE
    const { user } = state
    return {
      user,
      payment_type,
      type,
    }
  }
)(WizardFormSubmit)
export default WizardFormSubmit
