import React, { Component } from 'react';
import { connect } from 'react-redux'
//import PropTypes from 'prop-types';
import WizardFormFirstPage from './Forms/WizardFormFirstPage';
import WizardFormSecondPage from './Forms/WizardFormSecondPage';
import WizardFormThirdPage from './Forms/WizardFormThirdPage';
import WizardFormSubmit from './Forms/WizardFormSubmit';


import { setTimeout } from 'timers';
import {FormattedMessage} from 'react-intl';
//import {setLanguage} from './reducer/action';
//import {configuration} from './configuration'

import {
  //formValueSelector,
  getFormValues
} from 'redux-form'
let lastScrollY = 0;
let ticking = false;
let direction = 0;
let scroll_state = null;
var KUTE = require("kute.js"); //grab the core
require("kute.js/kute-svg"); // Add SVG Plugin
require("kute.js/kute-css"); // Add CSS Plugin
require("kute.js/kute-attr"); // Add Attributes Plugin
require("kute.js/kute-text"); // Add Text Plugin
//const logo = require('./assets/images/pru-logo.png');
class WizardForm extends Component {
  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this.gotoPage = this.gotoPage.bind(this);
    this.state = {
      page: 1,
    }
  }
  firstPage() {
    this.setState({ page:  1 });
  }
  gotoPage(number){
    this.setState({ page:  number });
  }



  nextPage() {
    //KUTE.to('.content',{opacity:0},{duration: 1000,easing: 'easingCubicInOut'}).start(); // for the window
       
    let tween1 =  KUTE.to('.content',{opacity:0,translateX:-100},{duration: 180,easing: 'easingCubicInOut'})
    let tween2 =  KUTE.to('.content',{opacity:0,translateX:100},{duration: 20,easing: 'easingCubicInOut'})
    let tween3 =  KUTE.to('.content',{opacity:1,translateX:0},{duration: 200,easing: 'easingCubicInOut'})
     tween1.start();
     tween1.chain(tween2); 
     tween2.chain(tween3); 
/*     let tween1 =  KUTE.to('.content',{svgTransform : { scale: 1.5}},{yoyo: true,duration: 200,easing: 'easingCubicInOut'})
    let tween2 =  KUTE.to('.fa-male>path',{svgTransform : { scale: 1}},{yoyo: true,duration: 200,easing: 'easingCubicInOut'})
     tween1.start();
     tween1.chain(tween2); */
    setTimeout(() => {
      this.setState({ page: this.state.page + 1 });
    }, 200);




    
  }
 
  previousPage() {
    let tween1 =  KUTE.to('.content',{opacity:0,translateX:100},{duration: 180,easing: 'easingCubicInOut'})
    let tween2 =  KUTE.to('.content',{opacity:0,translateX:-100},{duration: 20,easing: 'easingCubicInOut'})
    let tween3 =  KUTE.to('.content',{opacity:1,translateX:0},{duration: 200,easing: 'easingCubicInOut'})
     tween1.start();
     tween1.chain(tween2); 
     tween2.chain(tween3); 
/*     let tween1 =  KUTE.to('.content',{svgTransform : { scale: 1.5}},{yoyo: true,duration: 200,easing: 'easingCubicInOut'})
    let tween2 =  KUTE.to('.fa-male>path',{svgTransform : { scale: 1}},{yoyo: true,duration: 200,easing: 'easingCubicInOut'})
     tween1.start();
     tween1.chain(tween2); */
    setTimeout(() => {
      this.setState({ page: this.state.page - 1 });
    }, 200);
  }
  componentWillMount(){
    window.removeEventListener('scroll', this.handleScroll);
  }
  componentDidMount(){
    window.addEventListener('scroll', this.handleScroll);
/*     setTimeout(() => {
      // or a more complex .fromTo() example with the two new options
      var myMultiTween2 = KUTE.allFromTo(
       '.form-body .field',
       {translate:[0,150],opacity:0},
       {translate:[0,0],opacity:1},
       { offset: 200,delay: 500, easing:"easingBackOut" }
     ).start();
     
}, 1000); */
   
  }
  handleScroll = () => {
  
    lastScrollY = window.scrollY;
    if (!ticking) {
      window.requestAnimationFrame(() => {
        //console.log(scroll_state)
        if(lastScrollY <= 0 ){
          document.body.classList.remove("scroll-up");
          document.body.classList.remove("scroll-down");
        }else{
          if(lastScrollY > direction+20){
            // this.nav.current.style.display = "none";
             //KUTE.to(this.nav.current,{top:'-81px'},{yoyo: true,duration: 200,easing: 'easingCubicInOut'}).start();
            
             if(scroll_state!=="down"){
               //console.log("Set")
               scroll_state = "down"
               document.body.classList.add("scroll-down");
               document.body.classList.remove("scroll-up");
   
               /* this.nav.current.classList.add("scroll-down");
               this.nav.current.classList.remove("scroll-up"); */
             }
             direction = lastScrollY
   
           }
   
           if(lastScrollY < direction-20){
             
            // this.nav.current.style.display = "block";
           // KUTE.to(this.nav.current,{top:'0'},{yoyo: true,duration: 200,easing: 'easingCubicInOut'}).start();
           
           if(scroll_state!=="up"){
             //console.log("Set")
             scroll_state = "up"
             document.body.classList.remove("scroll-down");
             document.body.classList.add("scroll-up");
   
           }
   
           direction = lastScrollY
           }
   
        }
          
        ticking = false;
      });
   
      ticking = true;
    }
  };
  render() {
    const { onSubmit } = this.props;
    const { page } = this.state;
   // console.log(this.props.form)
   
    return (
      <div className={"page-"+page+" "+this.props.user.language}>
        {/* <header className="site-header">
          <div className="container upper-header has-text-centered">
            <img src={logo} className="pru-logo" onClick={() => this.toggleLanguage()} alt="Logo" />
          
            <div className="language-switcher">
            <span onClick={() => this.toggleLanguage()}>{this.props.user.language==="th"? "EN": "TH"}</span>
          </div>
          </div>
         
        </header> */}
{/* 
        <Header clickLanguage={()=>this.toggleLanguage()} language={this.props.user.language}/>
 */}

        <div className="container body">
          {/*       <ul id="progressbar">
          <li className={page>=1?"active": ""}>User Info</li>
          <li className={page>=2?"active": ""}>Clame data</li>
          <li className={page>=3?"active": ""}>Submit</li>
        </ul> */}


          <div className="intro-text"><h2 className="intro-text-title has-text-centered"><FormattedMessage id="app.title" /></h2>
            <p className="intro-text has-text-centered"><span>เพื่อความสะดวกในการเรียกร้องสินใหมท่านสามารถกรอกรายละเอียดการเรียกร้องค่าสินใหมตามด้านล่างพร้อมแนบเอกสารประกอบการพิจารณา</span></p></div>

        
        {page < 4 &&
          <ul className="steps has-content-centered is-horizontal">
            <li className={page === 1 ? "steps-segment is-active" : "steps-segment"}>
              <span className="steps-marker">1</span>
              <div className="steps-content">
                <p><FormattedMessage id="app.profile" /></p>
              </div>
            </li>
            <li className={page === 2 ? "steps-segment is-active" : "steps-segment"}>
              <span className="steps-marker">2</span>
              <div className="steps-content">
                <p><FormattedMessage id="app.claims" /></p>
              </div>
            </li>
            <li className={page === 3 ? "steps-segment is-active" : "steps-segment"}>
              <span className="steps-marker">3</span>
              <div className="steps-content">
                <p><FormattedMessage id="app.payment" /></p>
              </div>
            </li>
          </ul>
        }
          <div className="content">
            {page === 1 && <WizardFormFirstPage onSubmit={this.nextPage} />}
            {page === 2 &&
              <WizardFormSecondPage
                previousPage={this.previousPage}
                gotoPage={this.gotoPage}
                onSubmit={this.nextPage}
              />}
            {page === 3 &&
              <WizardFormThirdPage
                previousPage={this.previousPage}
                onSubmit={this.nextPage}
              />}
            {page === 4 &&
              <WizardFormSubmit
                previousPage={this.previousPage}
                onSubmit={onSubmit}
              />}
              
          </div>
        </div>
        {page === 5 &&
          <div className="modal is-active">
          <div className="modal-background"></div>
          <div className="modal-content">
          <WizardFormThirdPage
                previousPage={this.previousPage}
                onSubmit={onSubmit}
              />
          </div>
          <button className="modal-close is-large" aria-label="close"></button>
        </div> 
              }           
         
                 
      </div>
    );
  }
}
/* WizardForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
}; */
/* const mapStateToProps = state => {
  const { user } = state
  return {user}
} */
const mapStateToProps = state => {
  //const selector = formValueSelector('wizard') // <-- same as form name
  const { user } = state
  return {
    user,
    form: getFormValues('wizard')(state),
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    //setLanguage:(lang) => dispatch(setLanguage(lang))
  };
}
//export default WizardForm;
export default connect(mapStateToProps, mapDispatchToProps)(WizardForm)
