import React from "react";
const item = (props) => {
  return (
    <li className="contact" onClick={props.selected(props.index)}>
      <div className="contact-info">
        <div className="contact-name"> {props.index+1} : {props.code} : {props.name}</div>
      </div>
    </li>
  );
};
/* class item extends Component {
    constructor ( props, context ) {
		super( props, context );
		this.state = {
			selected: false,
			hover: false,
			results: this.props.results,
			loading: this.props.loading
		};
	}
    select() {
        //event.preventDefault();
        //console.log(this.props.index)
        this.props.selected(this.props.index)
        //this.props.onSearch
    }
    render() {
      return (
        <li className="contact" onClick={this.select.bind(this)}>
          <div className="contact-info">
            <div className="contact-name"> {this.props.index+1} : {this.props.code} : {this.props.name}</div>
          </div>
        </li>
      );
    }
} */
  export default item;