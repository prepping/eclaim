import React from 'react'
import {FormattedMessage,intlShape, injectIntl, defineMessages} from 'react-intl';
import AddressFormTypeahead from 'react-thailand-address-typeahead';

const ThaiAddressInput = ({ intl, input,icon, label,placeholder, type, style, meta: { touched, error } }) => {
  

  //console.log(icon)

  let placeholder_text = ""

  

  if(placeholder){
    
    if(placeholder.props){
      placeholder_text = intl.formatMessage({id: placeholder.props.id});
    }else{
      placeholder_text = placeholder
    }

   

  }
  
  return (
        <div className="thai-address-wrap " style={style}>
          <AddressFormTypeahead
           onAddressSelected={(addressObject) => console.log(addressObject)} 
          />
           {touched && error && <p className="help is-danger">{error}</p>}
      </div>
)}




ThaiAddressInput.propTypes = {
  intl: intlShape.isRequired
}

export default injectIntl(ThaiAddressInput);

//export default renderField
