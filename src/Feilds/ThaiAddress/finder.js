/* 'use strict';
var JQL = require('jqljs');
import JQL from './JQL' */
//var JQL = require('./JQL');
/* var fieldsEnum = {
  DISTRICT: 'd',
  AMPHOE: 'a',
  PROVINCE: 'p',
  ZIPCODE: 'z'
}; */
/**
 * From jquery.Thailand.js line 30 - 128
 * Search result by FieldsType
 */
export function preprocess(data) {
//var preprocess = function preprocess(data) {
  if (!data[0].length) {
    // non-compacted database
    return data;
  }
  // compacted database in hierarchical form of:
  // [["province",[["amphur",[["district",["zip"...]]...]]...]]...]
  var expanded = [];
  data.forEach(function (provinceEntry) {
    var province = provinceEntry[0];
    var amphurList = provinceEntry[1];
    amphurList.forEach(function (amphurEntry) {
      var amphur = amphurEntry[0];
      var districtList = amphurEntry[1];
      districtList.forEach(function (districtEntry) {
        var district = districtEntry[0];
        var zipCodeList = districtEntry[1];
        zipCodeList.forEach(function (zipCode) {
          expanded.push({
            d: district,
            a: amphur,
            p: province,
            z: zipCode
          });
        });
      });
    });
  });
  return expanded;
};
/****LOAD DEFAULT DATA****/
/* async function loadAddress(){
const rawResponse = await fetch(`${process.env.PUBLIC_URL}/data.json`, {
  //return fetch('http://httpbin.org/post', {
    method: 'GET',
    //headers: h,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
const content = await rawResponse.json();
return  new JQL(preprocess(content));
//window.thaiAddress = new JQL(preprocess(content));
//console.log(window.thaiAddress)
  }
var DB=null;
loadAddress().then(data=>DB=data)
console.log(DB) */
//var DB = new JQL(preprocess(require('./data.json')));
//console.log(DB)
/* var resolveResultbyField = (type, searchStr)=> {
  var possibles = [];
  try {
    possibles = DB.select('*').where(type).match('^' + searchStr).orderBy(type).fetch();
  } catch (e) {
    return [];
  }
  return possibles;
}; */
 //exports.resolveResultbyField = resolveResultbyField;
//exports.fieldsEnum = fieldsEnum; 
//export  fieldsEnum;
export function resolveResultbyField(type, searchStr,language) {
  var possibles = [];
  try {
   // possibles = DB.select('*').where(type).match('^' + searchStr).orderBy(type).fetch();
    possibles =  window.thailandAddress[language].select('*').where(type).match('^' + searchStr).orderBy(type).fetch();
    //possibles = DB.select('*').where(type).match('^' + searchStr).orderBy(type).fetch();
  } catch (e) {
    return [];
  }
  return possibles;
};