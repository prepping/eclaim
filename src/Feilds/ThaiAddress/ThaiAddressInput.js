 /**
  * @name THAIADDRESSINPUT
  * @version 1.0.0
  * @update Nov 07, 2018
  * @author ixPingxi
  * @license WTFPL v.2 - http://www.wtfpl.net/
  **/
import React from 'react'
//import {FormattedMessage,intlShape, injectIntl, defineMessages} from 'react-intl';
//import AddressFormTypeahead from 'react-thailand-address-typeahead';
//import AddressFormTypeahead from './finder';
import { maxlenghtCheck , matchCheck} from '../validate';
import './style.css'
//var _finder = require('./finder.js');
var JQL = require('./JQL');
var _finder = require('./finder');
window.thailandAddress = [];
//const ThaiAddress = require("./ThaiAddress.json")
/*******Attribute****
 * 
 *  limit
 *  language
 *  addressType
            DISTRICT: 'd',
            AMPHOE: 'a',
            PROVINCE: 'p',
            ZIPCODE: 'z'
 *  
 * 
 * 
 * 
 * 
 * **/
export class ThaiAddressInput extends React.Component {
    //state = { src: null,errorFilesize:null };
  
    constructor(props) {
      super(props);
      this.state = {
        result : [],
        currentLanguage : null
  
      };
      this.onChange = this.onChange.bind(this);
      this.onBlur = this.onBlur.bind(this);
      this.onFocus = this.onFocus.bind(this);
  
  }
  
   onChange(e){
    const {input,addressType,language,maxlenght,match} = this.props
    //if( maxlenght && e.target.value.length > parseInt(maxlenght,10)) return false;
    let value = e.target.value
    if(match && !matchCheck(value,match))return  false
    if(maxlenght && !maxlenghtCheck(value,maxlenght))return  false
    //return  input.onChange(value)
    if(!window.thailandAddress[language])  this.loadAddress(language);
    if(value.trim().length >= 2){
    let test =   _finder.resolveResultbyField(addressType,value.trim(),language? language : "th")
      //console.log(window.thailandAddress)
    this.setState({result:test})
    //console.log(e.target.value)
    }else{
      this.setState({result:[]})
    }
    input.onChange(value)
  }
   onBlur(e) {
   
    e.persist();
    setTimeout(() => {
      e.target.parentNode.classList.remove('focus')
    }, 200);
/*     if(this.props.user.occ != null){
        this.setState({searchQuery : this.state.list[this.state.selected].occu_description}) 
    }else{
        this.setState({searchQuery : "", selected : null})
    } */
  }
  async onFocus(e) {
     //console.log(e.target.parentNode.)
     e.target.parentNode.classList.add('focus')
     //e.target.classList.remove('selected')
    // e.target.classList.remove('selected')
     //e.focus();
    //document.querySelector(".ac-input").focus();
/*     this.setState({
        searchQuery : "",
        contacts : this.state.list
    })  */
    const {input,addressType,language} = this.props
    if(!window.thailandAddress[language])  await this.loadAddress(language);
    if(input.value.length >= 2 && this.state.result.length===0){
    let test =   _finder.resolveResultbyField(addressType,input.value,language? language : "th")
      //console.log(window.thailandAddress)
    this.setState({result:test})
    //console.log(e.target.value)
    }else if(this.currentLanguage !== language && input.value.length >= 2){
      let test =   _finder.resolveResultbyField(addressType,input.value,language? language : "th")
      //console.log(window.thailandAddress)
    this.setState({result:test})
    }
  }
  clearInput(e) {
     //console.log( e.target)
    e.target.parentNode.childNodes[0].focus();
    let  { input } = this.props
    input.onChange("")
    
  }
  onSelect(e,item){

    //console.log(e)
    //e.persist();
   let  { callback } = this.props
    //console.log(item)
    //console.log(e.target.parentNode.parentNode.childNodes[0])
/*     this.setState({
        searchQuery : item.d,
    }); */
    callback(item)
    //input.onChange(item.d)
    //setTimeout(() => {
      e.target.parentNode.childNodes[0].classList.remove('focus')
    //}, 100);
    
  }
  async loadAddress(language){
    //console.log(language)
    const rawResponse = await fetch(this.props.dataPath, {
      //return fetch('http://httpbin.org/post', {
        method: 'GET',
        //headers: h,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        //body: JSON.stringify({ "base64Img": base64Img.replace(/^data.*.base64,/i, "")})
      })/* .then(function(response) {
        console.log(response)
       }) */
    const content = await rawResponse.json();
    //console.log(content)
    //console.log(_finder.preprocess(content))
    window.thailandAddress[language] = new JQL(_finder.preprocess(content));
       this.setState({
        currentLanguage : language
       })
   // console.log(window.thailandAddress)
  }
    
      render() {
          //const { input } = this.props
          const { input,icon, addressType,label,placeholder, limit, type, style, meta: { touched, error } } = this.props
        this.state.result && this.state.result.map((item,index) =>{
            //console.log(item.d)
             return item;
        })
        //if(this.state.ready){
      
         // let searchQuery = this.state.searchQuery
      
      
      
      
      
      
          
return (
         
<div className="field" style={style}>
    {label && <label className="label">{label}</label>}
    
    <div className={"control has-icons-right autocomplete-container"+ (icon? "has-icons-left": "") }>
    
      <input className={touched && error ?"input is-danger":"input"} type={type} value={input.value} onChange={this.onChange} placeholder={placeholder}  onBlur={this.onBlur} onFocus={this.onFocus}/>
      
      
      {input.value.length > 1 &&
      <ul className={"contacts-list "+ addressType}>
          {
           this.state.result.length > 0 ? this.state.result.slice(0, limit ? limit : this.state.result.length ).map((item,index)=> {
              return (<li onClick={(e)=>this.onSelect(e,item)} key={index}>
                <span>{item.d}</span>
                <span>{item.a}</span>
                <span>{item.p}</span>
                <span>{item.z}</span>
              </li>)
            }) : <li className="contact" >
            ไม่พบข้อมูล (No result)
            </li>
          
          }
        </ul>
        }
      

      {input.value.length > 1 &&
        <svg className="close" onClick={this.clearInput.bind(this)}  viewBox="0 0 40 40" width="100%" height="100%"><path d="M16.228 20L1.886 5.657 0 3.772 3.772 0l1.885 1.886L20 16.228 34.343 1.886 36.228 0 40 3.772l-1.886 1.885L23.772 20l14.342 14.343L40 36.228 36.228 40l-1.885-1.886L20 23.772 5.657 38.114 3.772 40 0 36.228l1.886-1.885L16.228 20z" fillRule="evenodd"></path></svg>
      }
        

 
      {icon && <span className="icon is-small is-left">
      <i className={"fas fa-"+icon}></i>
    </span>}
      
      {touched && !error  && input.value && <span className="icon is-small is-right">
        <i className="fas fa-check"></i>
      </span>}
    </div>
    {touched && error && <p className="help is-danger">{error}</p>}
  
</div>
  
  
  
  
        )
      }
    }
  
/* ThaiAddressInput.propTypes = {
  intl: intlShape.isRequired
} */
export default ThaiAddressInput;
//export default renderField
