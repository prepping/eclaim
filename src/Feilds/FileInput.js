
import React from "react";
import {FormattedMessage} from 'react-intl';
import '../Feilds/fileInput.css'
export class FileInput extends React.Component {
  state = { src: null,errorFilesize:null };
  constructor(props) {
    super(props);
    this.state = {
      src: null,errorFilesize:null 
    };
}
    getBase64 = (file) => {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
      });
    }
  
    onFileChange = async (e) => {
      const { input, maxSize } = this.props
      //const _maxSize = maxSize || 2048000;
      const _maxSize = maxSize || 2048000;
      const targetFile = e.target.files[0]
      console.log(targetFile)
      if(targetFile && _maxSize && targetFile.size <= _maxSize ){
              
        const val = await this.getBase64(targetFile)
        input.onChange(val)
        this.setState({errorFilesize:false})
      }else{
        console.log("over file size")
        input.onChange(null)
        this.setState({errorFilesize:true})
      }
     // console.log(e.target.files[0])
      //console.log(e.target.files[0].type)
      //console.log(e.target.files[0].size)
      //console.log(this.state)
    
      
    }
  
    render() {
        //const { input } = this.props
        const { input, label, type, meta: { touched, error } } = this.props
      return (
       
<div>  
  
  <div className="input-file-container">  
        <img src={input.value} style={{width:"50px"}}/>
        <div>
          <input
            className="input-file"
            type="file"
            /* accept="image/*" */
            onChange={this.onFileChange}
          />
          <label tabIndex="0" htmlFor="my-file" className="input-file-trigger">Select a file...</label>
          
          </div>
          
          {touched && error && <p className="help is-danger">{error}</p>}
          {this.state.errorFilesize && <p className="help is-danger"><FormattedMessage id="app.error.limitFileSize"/></p>}
  </div>
</div>
      )
    }
  }
  export default FileInput
/* <Field
                      name="files"
                      label={<FormattedMessage id="app.user.profile" />}
                      textBtn={<FormattedMessage id="app.browse" />}
                      placeholder={<FormattedMessage id="app.user.profile" />}
                      accept="image/jpeg, image/png, image/gif, image/bmp"
                      component={FileInput}
                      type="file"
 /> */
