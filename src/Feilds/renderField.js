import React from 'react'
import { injectIntl} from 'react-intl';
import { maxlenghtCheck , matchCheck} from './validate';
const renderField = ({ intl, pattern,maxlenght, match, input,icon, label,placeholder, type, style, meta: { touched, error } }) => {
  
  //console.log(icon)
  let placeholder_text = ""
  
  if(placeholder){
    
    if(placeholder.props){
      placeholder_text = intl.formatMessage({id: placeholder.props.id});
    }else{
      placeholder_text = placeholder
    }
   
  }
const handleChange = (e)=>{
    let value = e.target.value;
    //let{maxlenght,match} = this.props
    //console.log(maxlenght)
    //console.log(value.length)
   // console.log(e.target.getAttribute('type'))
/*     if( maxlenght && value.length <= parseInt(maxlenght,10)){
      input.onChange(e.target.value)
    }else if(!maxlenght){
      input.onChange(e.target.value)
    } */
    if(match && !matchCheck(value,match))return  false
    if(maxlenght && !maxlenghtCheck(value,maxlenght))return  false
    return  input.onChange(value)
  }
/*   const  maxlenghtCheck = function(value,maxlenght){
    if( maxlenght && value.length <= parseInt(maxlenght,10)){
      return true
    }else if(!maxlenght){
      return true
    }
    return false;
  }
  const matchCheck = function(value,match){
    if( match && match.test(value)){
      return true
    }else if(!match){
      return true
    }
    return false;
  } */
  
  return (
 <div className="field" style={style}>
    {label && <label className="label">{label}</label>}
    
    <div className={"control has-icons-right "+ (icon? "has-icons-left": "") }>
      <input className={touched && error ?"input  is-danger":"input"} {...input}  onChange={ (event)=>{ handleChange(event)} }  placeholder={placeholder_text} type={type}/>
     
      {icon && <span className="icon is-small is-left">
      <i className={"fas fa-"+icon}></i>
    </span>}
      
      {touched && !error  && input.value && <span className="icon is-small is-right">
        <i className="fas fa-check"></i>
      </span>}
    </div>
    {touched && error && <p className="help is-danger">{error}</p>}
  
</div>
)}
export default injectIntl(renderField);
//export default renderField
