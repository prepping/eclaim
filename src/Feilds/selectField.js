import React from 'react'
import {injectIntl} from 'react-intl';
const selectField = ({ intl, input, options, icon, label,placeholder, type, style, meta: { touched, error } }) => {
  
  //console.log(icon)
  let placeholder_text = ""
  
  if(placeholder){
    
    if(placeholder.props){
      placeholder_text = intl.formatMessage({id: placeholder.props.id});
    }else{
      placeholder_text = placeholder
    }
   
  }
  
  return (
 <div className="field" style={style}>
    {label && <label className="label">{label}</label>}
    
    <div className="select">
    
    <select {...input}>
    <option value="">{placeholder_text}</option>
    {options.map(val => {
      
      let text = !val.props ? val : intl.formatMessage({id: val.props.id})
      return (
      <option value={text} key={text}> {text} </option>
    )})}
    </select>
    </div>
    {touched && error && <p className="help is-danger">{error}</p>}
    <br/><br/>
</div>
)}
export default injectIntl(selectField);
//export default renderField
