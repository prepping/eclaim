import map from "lodash/map";
import React from "react";
import DropZone from "react-dropzone";
import '../Feilds/dropzoneFeild.css'
//import { MdCloudUpload } from "react-icons/md";
/* const renderImagePreview = imagefile => {
  return map(imagefile, ({ name, preview, size }) => [
    <li key={size}>
      <img
        style={{ display: "block", margin: "auto", paddingTop: "10%" }}
        src={preview}
        alt={name}
        height="220px"
        width="200px"
      />
    </li>,
    <li style={{ textAlign: "center" }} key="imageDetails">
      {name} - {size} bytes
    </li>
  ]);
}; */
export class dropzoneField extends React.Component {
  //state = { src: null,errorFilesize:null };
  constructor(props) {
    super(props);
    this.state = {
      src: null,errorFilesize:null ,
      inputTemp : props.input.value || [],
      sumFileSize : 0,
      //errorFileSize : false
    };
   // this.gotoPage = this.gotoPage.bind(this);
}
bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes === 0) return '0 Byte';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)),10);
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};
  
onFileChange = async (e) => {
      const { input , multiple } = this.props
      //let inputTemp = [...input.value,e[0]];
/*       let error = null
      let newAddFilesSum = this.sumFileSize(e)/1024;
      console.log(newAddFilesSum+"/"+maxSize);
      if(maxSize && newAddFilesSum + this.state.sumFileSize <= maxSize){
 */
        //console.log(multiple)
        
        if(multiple){
        await this.setState({
          inputTemp :[...this.state.inputTemp,...e]
         })
        }else{
          await this.setState({
            inputTemp :[...e]
           })
        }
        //console.log(e[0])
        //*inputTemp.concat(inputTemp); 
  
        await input.onChange(this.state.inputTemp);
       // console.log(typeof this.props.returnSize === "function")
  
          if(typeof this.props.returnSize === "function"){
          this.props.returnSize(this.sumFileSize())
         }
         //this.props.returnSize(this.sumFileSize())
      /* }else{
        
        console.log("Error over file size");
        this.setState({
          errorFileSize : true
         })
         
      }
 */
   
     
      
    }
    sumFileSize = (files)=>{
      let sumFileSize = 0
   
      if(!files){
         //Return sum from All file in this feild 
         sumFileSize = this.state.inputTemp.reduce((all,item,index)=>{
          all += item.size
          return all;
        },0) 
  
        //console.log("sumFileSize : "+sumFileSize)
        this.setState({
          sumFileSize :sumFileSize
         })
      }else{
        sumFileSize = files.reduce((all,item,index)=>{
          all += item.size
          return all;
        },0) 
        //Return sum from input in this feild 
      }
       return sumFileSize
    }
    handleClickRemove = async (preview, event) => {
      const { input } = this.props
      //console.log(name)
     /*  const index = this.state.inputTemp.indexOf(event);
      console.log(event) */
      //console.log(this.state.inputTemp[index])
      //console.log(index)
      //let list = [...this.state.inputTemp]
      let list = [...input.value]
     // console.log(list)
      //let final = list.splice(index, 1);
      let final = list.filter( (e, i) =>{
      	//return i !== index;
      	return e.preview !== preview;
      })
      //console.log(final) 
      
      await this.setState({inputTemp :final }) 
      await input.onChange(final)
       //this.sumFileSize();
       if(typeof this.props.returnSize === "function"){
        this.props.returnSize(this.sumFileSize())
       }
      
    }
    componentDidMount(){
      if(typeof this.props.returnSize === "function"){
        this.props.returnSize(this.sumFileSize())
       }
    }
    renderImagePreview = imagefile => {
      
      return map(imagefile, ({ name, preview, size, type },index) => {
        
        //console.log(type.includes("image"));
        let  img = <span className="icon-file-type"><i className="fas fa-file"></i></span>
        
        if(type.includes("image")){
         img = <img src={preview} alt={name}/>
        }else if(type.includes("pdf")){
          img = <span className="icon-file-type"><i className="fas fa-file-pdf"></i></span>
        
        }
        
        return [
        <li key={name} >
          {img}
           { <span className="file-details">
          {name} <br/> ( {this.bytesToSize(size)} )
        </span> }
<span className="icon has-text-danger remove-file" onClick={this.handleClickRemove.bind(this, preview)}>
  <i className="fas fa-times-circle"></i>
</span>
</li>,
       
      ]});
    };
  
    render() {
        //const { input } = this.props
        const {   
          //handleOnDrop,
          input,
          //imagefile,
          label,
          style,
          textBtn,
          multiple,
          accept,
          meta: { error, touched }} = this.props
          //if(this.props.multiple)
      return (
      
        <div className="field" style={style}>
          {label && <label className="label">{label}</label>}
        <DropZone
          {...input}
          accept={accept}
          className="upload-container"
          multiple={multiple}
          onDrop={file => {this.onFileChange(file); }}
          //onChange={file => input.onChange(file)}
          //style={{ height: 300, width: 500,backgroundColor: "#efebeb" }}
        >
        <a className="button is-outlined">{textBtn}</a>
    
{/*           {imagefile && imagefile.length > 0 ? (
            <ul
              style={{
                backgroundColor: "#efebeb",
                listStyleType: "none",
                height: 300,
                width: 470
              }}
            >
              {renderImagePreview(imagefile)}
            </ul>
          ) : (
            <div
              style={{
                textAlign: "center",
                backgroundColor: "#efebeb",
                height: "100%",
                width: "100%",
                borderRadius: 5
              }}
            >
              <div style={{ paddingTop: 70 }}>
                
              </div>
              <p>Click or drag image file to this area to upload.</p>
            </div>
          )} */}
        </DropZone>
          <ul className="image-upload-list">
        {this.renderImagePreview(this.state.inputTemp)}
      </ul>
        {touched && error && <div style={{ color: "red" }}>{error}</div>}
        {this.state.errorFileSize && <div style={{ color: "red" }}>errorFileSize</div>}
      </div>
      
      
      
      
      )
    }
  }
  export default dropzoneField;
