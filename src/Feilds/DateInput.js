
import React from "react";

//import {FormattedMessage,intlShape, injectIntl, defineMessages} from 'react-intl';


//Date Picker
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {} from 'moment/locale/th';






import 'react-datepicker/dist/react-datepicker.css';

var dateFormat = "DD/MM/"

export class DateInput extends React.Component {

  state = { 
    locale: this.props.lang ,
    value : this.props.input.value 
  };

  componentWillMount(){

    //moment(date).format("DD/MM/YYYY");
   // console.log(moment(this.props.input.value ).format("DD/MM/YYYY"))

   //console.log(this.props.input.value)

   //console.log(moment(this.props.input.value,"DD/MM/YYYY").format("DD/MM/YYYY"))

    //if(this.props.lang==="th" && this.props.input.value){

      //this.setState({value:moment(this.props.input.value ).format("DD/MM/YYYY")})

      //console.log(moment(this.props.input.value ))

    /*   const date = moment(this.props.input.value ).format("DD/MM/")
      let year = moment(this.props.input.value ).format("YYYY")
      year += 543
      const value =date+year

      this.setState({value:value}) */


     // } 

      


}


componentWillReceiveProps(newProps) {

 


}


componentDidMount(){
  document.getElementById("dtpicker").setAttribute("autocomplete","off");
  //document.getElementById("inputdefault").setAttribute("autocomplete","off");
  
}
  
onChange = async (e) => {
      const { input } = this.props

      //console.log(e.format("DD/MM/YYYY"))

      //s.value =moment(date).format("DD/MM/YYYY");

      const date = e.format("DD/MM/")
      let year = e.format("YYYY")
      const value =date+year

      if (date) {

        if(this.props.lang==="th"){
          input.onChange(date+(year-543))
        }else{
          input.onChange(value)
        }
      
        this.setState({value:value})

      } else {

        input.onChange(null)

      }
    }


  handleChangeRaw = (value) => {
    
      //console.log(value)
  
      let s=document.getElementById("dtpicker")
      //s.value =moment(date).format("DD/MM/YYYY");
      //this.setState({ birthday:date });
  
      var date = moment(value, dateFormat);
      if (date.isValid() === true) {
          this.setAgeFromDatePicker(value)
      }else{
          s.value ="";
  
      }
  
  
      
    }


    showDate(){

      if(this.props.input.value){
        return moment(this.props.input.value,"DD/MM/YYYY").format("DD/MM/YYYY")
      }else{
        return null;
      }
      

    }

    render() {
      const { label, meta: { touched, error } } = this.props

      var yearadd = 0;
      if(this.props.lang==="th"){
         yearadd = 543;
        }


        window.datePicker_yearAdd= yearadd
        //console.log(window.datePicker_yearAdd)
          const thaiSolarFormat = 'YYYY';
          const checkThaiSolarFormat = format => {
           return /YYYY/.test(format);
         };
         
          const convertThaiSolarYear = (year) => {
           return parseInt(year, 10) + yearadd;
         }
     
         const format = moment.fn.format;
         //const year = moment.fn.year;
     
           moment.fn.format = function (str) {
           if (str) {
             if (checkThaiSolarFormat(str)) {
               str = str.replace(thaiSolarFormat, convertThaiSolarYear(this.get('year')));
             }
           }
     
           return format.call(this, str);
           } 

           //let value = ""

         
           /* if(this.state.locale !== this.props.lang){
             input.onChange(null)
             console.log(555)
             this.setState({value:null})

             value = null
              this.state.locale = this.props.lang
              
            }else{

              value = this.state.value

            } */


           
           // console.log(input.value)


      return (
          <div>

<div className="field">
    <label className="label">{label}</label>
    <div className="control has-icons-left has-icons-right" style={{zIndex:999}}>
              <DatePicker
                className = {touched && error ?"input is-danger":"input"}
                onChange={this.onChange}
                //selected={input.value}
                //value = {this.state.value}
                openToDate={this.props.input.value !=="" ? moment(this.props.input.value, 'DD/MM/YYYY') : null}
                value = {this.showDate()}
                minDate={moment().locale('th').subtract(99, "year")}
                maxDate={moment().locale('th').subtract(0, "year")}
                peekNextMonth
                showYearDropdown
                autocomplete="false"
                dropdownMode="select"
                placeholderText={"DD/MM/YYYY"}
                popperPlacement="bottom"
                dateFormat={"DD/MM/YYYY"}
                locale={this.props.lang}
                //onBlur={e=>this.handleBegDateBlur(e)}
                id="dtpicker"
                //onChangeRaw={(e)=>this.handleChangeRaw(e)}
                />   
    
      <span className="icon is-small is-left">
        <i className="fas fa-calendar-alt"></i>
      </span>
      
      {this.state.value && <span className="icon is-small is-right">
        <i className="fas fa-check"></i>
      </span>}



    </div>

    {touched && error && <p className="help is-danger">{error}</p>}
  
</div>






        </div>
      )
    }
  }

  export default DateInput