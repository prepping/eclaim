// Action
export const setLanguage = (language) => ({
    type: 'SETLANGUAGE', language,
    logged:true
  })