import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';


let translateSource = {
  "en":require('../assets/translate/en.json'),
  "th":require('../assets/translate/th.json'),
}



//console.log(translateSource);




// This function will map the current redux state to the props for the component that it is "connected" to.
// When the state of the redux store changes, this function will be called, if the props that come out of
// this function are different, then the component that is wrapped is re-rendered.
function mapStateToProps(state) {
//console.log(state);
  const { language } = state.user;
  //let messages = {"app.hello_world":"sdfsdf"}
  let messages = translateSource[language]
  return { locale: language, messages};
}

export default connect(mapStateToProps)(IntlProvider);