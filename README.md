# eClaim

# Version Log
- 1.6.3 : Clean some code
- 1.6.2 : Add featured : Timeout when post data.
- 1.6.1 : Improve some UI/UX
- 1.6.0 : Update translation and Thailand Address
- 1.5.5 : Thailand Address english version
- 1.5.0 : Thailand Address auto complete module.
- ...

# New Features!
  - Thailand adress auto complete.


# Fixed
- Thai Year for [react-datepicker]
Path : /node_modules/react-datepicker/es/index.js
```
//Line : 733
options.push(React.createElement(
          "option",
          { key: i, value: i },
          i + window.yearAdd
        ));
```  

